#!/usr/bin/perl 
use LWP::Simple;
use Cwd;
use Date::Parse;

$cwd = getcwd();

while (true) {
    sleep 1;

    # Determine attendance or voting mode

    open( MODE, "<$cwd/settings/mode" );
    while (<MODE>) {
        chomp($_);
        $mode = $_;
        print "mode is $mode.\n";
    }
    close(MODE);

    # Determine session ID
    open( SESSION, "<$cwd/settings/current_session" );
    while (<SESSION>) {
        chomp($_);
        $sID = $_;
        print "session is $sID.\n";
    }
    unless ( $sID >= 1000 && $sID <= 9999 ) {
        $sID = "1000";
    }
    close(SESSION);

    # Determine vendor ID
    open( VENDOR, "<$cwd/settings/current_vendor" );
    while (<VENDOR>) {
        chomp($_);
        $vID = $_;
        print "vendor is $vID.\n";
    }
    unless ( $vID >= 1000 && $vID <= 9999 ) {
        $vID = "1000";
    }
    close(VENDOR);

    chomp($mode);

    if ( $mode eq "attendance" ) {
        $retval = check_to_run();
        if ($retval) {
            print "Clear to run.\n";
            process_scans();
        }
        else {
            sleep 5;
        }
    }
    else {
        process_scans();
    }

}

sub process_scans {
    print "processing scans...\n";
    $scansdir = "$cwd/scans";
    opendir( DIR, $scansdir ) or die $!;
    while ( my $scanfile = readdir(DIR) ) {
        if ( $mode eq "attendance" ) {
            $retval = system("$cwd/chargedetect.py");
            if (( $retval ne 0 ) && (! -e "$cwd/settings/attendanceoverride")) {
                print "We are not charging and not overridden.   Waiting.\n";
                sleep 5;
                next;
            }

        }

        # Ignoring files and folders
        next if ( $scanfile =~ m/^\./ );
        next if ( $scanfile =~ m/^error/ );
        next if ( $scanfile =~ m/^done/ );
        next if ( $scanfile =~ m/^README/ );

        open( FILE, "$scansdir/$scanfile" )
          || die "unable to open \"$scansdir/$scanfile\"";
        while (<FILE>) {
            chomp($_);
            $aID = $_;
            $aID =~ s/AttendeeID://o;
        }
        close(FILE);
        call_url( $aID, "$scanfile" );
    }

}

sub call_url {

    my $aID  = $_[0];
    my $file = $_[1];

    if ( $mode eq "attendance" ) {
        $url =
"https://itweb.mst.edu/cgi-bin/cgiwrap/it_rtd/recordAttendance.pl?sessionID=$sID&attendeeID=$aID";
    }
    elsif ( $mode eq "voting" ) {
        $url =
"https://itweb.mst.edu/cgi-bin/cgiwrap/it_rtd/recordVote.pl?vendorID=$vID&attendeeID=$aID";
    }
    else {
        $url =
"https://itweb.mst.edu/cgi-bin/cgiwrap/it_rtd/recordAttendance.pl?sessionID=$sID&attendeeID=$aID";
    }

    #print "Calling Web URL.\n";
    print "$url\n";
    $retdata = get $url;
    chomp($retdata);
    print "retdata is $retdata.\n";

    if ( $retdata ne "1" ) {
        $datetime = localtime();
        chomp($datetime);
        $retdata =~ s/\n//g;
        open( ERRORFILE, ">>$cwd/errors.csv" );
        print ERRORFILE "$datetime, $aID, $sID, $url, $retdata\n";
        close(ERRORFILE);
        rename "$scansdir/$file",
          "$scansdir/error/$file.$mode.v$vID.s$sID.error";

    }
    else {
        print "success!\n";
        rename "$scansdir/$file", "$scansdir/done/$file.$mode.v$vID.s$sID.done";
    }

}

sub check_to_run {

    $datetime = localtime();
    $scantime = get_last_scan();
    $datetime = str2time($datetime);
    $scantime = str2time($scantime);
    $diff     = $datetime - $scantime;
    print "time difference is $diff seconds.\n";
    if (( $diff > 120 ) || (-e "$cwd/settings/attendanceoverride")) {

        # We are clear to run
        return 1;
    }
    else {
        return 0;
    }

}

sub get_last_scan {

    open( LASTSCAN, "<$cwd/settings/lastscan" );
    while (<LASTSCAN>) {
        $scantime = $_;
    }
    close(LASTSCAN);
    return $scantime;
}

#print "done.\n";

