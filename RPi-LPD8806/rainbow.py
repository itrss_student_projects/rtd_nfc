#!/usr/bin/python
from time import sleep
from raspledstrip.ledstrip import *
from raspledstrip.animation import *



num=8
led=LEDStrip(num)
led.all_off()

anim = RainbowCycle(led)
for i in range(512*3):
	anim.step()
	led.update()

led.fillOff()


led.all_off()



