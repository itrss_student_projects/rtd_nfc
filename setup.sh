#!/bin/bash
# This script will install everything for the NFC. 
# It expects everything to live in /home/pi/rtd_nfc.

#apt-get -y install pcscd pcsc-tools lipbcsc-perl libpam-pkcs11 libpcsclite1 python-dev
dpkg -i ./libnfc_1.7.1-1_armhf.nodoxygen.deb
mkdir /etc/pam_pkcs11/
cp /home/pi/rtd_nfc/card_eventmgr.conf /etc/pam_pkcs11/

sed -i /etc/modprobe.d/raspi-blacklist.conf -e "s/^blacklist[[:space:]]*spi-bcm2708.*/#blacklist spi-bcm2708/"
sudo modprobe spi-bcm2708

/usr/bin/perl -pi -e 's/^exit.*/\/home\/pi\/rtd_nfc\/autostart.sh\nexit 0/' /etc/rc.local
/usr/bin/perl -pi -e 's/^BLANK_TIME=30.*/BLANK_TIME=0/' /etc/kbd/config
