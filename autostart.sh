#!/bin/bash
sudo service pcscd start
setterm -blank 0
cd /home/pi/rtd_nfc/
rm settings/attendanceoverride
#sudo ./networkmonitor.sh &
./led_management.py &
./register_scans.pl &

until /usr/bin/card_eventmgr; do
	echo "card_eventmgr crashed with exit code $?.  Respawing..." >&2
	sleep 1
done
