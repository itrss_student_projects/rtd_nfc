#!/usr/bin/perl -w

use Chipcard::PCSC;
use Cwd;

$cwd = getcwd();

#reading_anim();

# Create the chipcard object
$hContext = new Chipcard::PCSC();
die("Can't create the PCSC object: $Chipcard::PCSC::errno\n")
  unless defined $hContext;

# Get a list of the readers on the system
@ReadersList = $hContext->ListReaders();
die("Can't get readers' list: $Chipcard::PCSC::errno\n")
  unless defined $ReadersList[0];

# Assign the first reader to the object
$hCard = new Chipcard::PCSC::Card( $hContext, $ReadersList[0] );
die("Can't connect: $Chipcard::PCSC::errno\n")
  unless defined $hCard;

# If everything connected properly...
if ($hCard) {




#Send the Select Applet APDU

#    ( $sw, $RecvData10 ) = $hCard->TransmitWithCheck(
#        "FF 00 00 00 0D D4 40 01 10 10 00 00 00 00 00 00 00 00",
#        "90 00" );
#
#    unless ($sw) {
#        flash_red();
#        exit 1;
#    }

$cmd = Chipcard::PCSC::ascii_to_array("FF B0 00 12 10");
$RecvData12 = $hCard->Transmit($cmd);
unless ($RecvData12){
flash_red();
        exit 1;
}
$RecvData12=Chipcard::PCSC::array_to_ascii($RecvData12);

$cmd = Chipcard::PCSC::ascii_to_array("FF B0 00 16 10");
$RecvData16 = $hCard->Transmit($cmd);
unless ($RecvData16){
flash_red();
	exit 1;
}
$RecvData16=Chipcard::PCSC::array_to_ascii($RecvData16);



$cmd = Chipcard::PCSC::ascii_to_array("FF B0 00 1A 10");
$RecvData1A = $hCard->Transmit($cmd);
unless ($RecvData1A) {

        flash_red();
        exit 1;
}


$RecvData1A=Chipcard::PCSC::array_to_ascii($RecvData1A);




$cmd = Chipcard::PCSC::ascii_to_array("FF B0 00 1E 10");
$RecvData1E = $hCard->Transmit($cmd);
unless ($RecvData1E) {
        flash_red();
        exit 1;
}
$RecvData1E=Chipcard::PCSC::array_to_ascii($RecvData1E);


# Honey badger don't give a shit about the APDU return code.  Cut it off.
$RecvData12=substr($RecvData12, 0, 48);
$RecvData16=substr($RecvData16, 0, 48);
$RecvData1A=substr($RecvData1A, 0, 48);
$RecvData1E=substr($RecvData1E, 0, 48);





    if ( length($RecvData1E) < 8 ) {
        flash_red();
        exit 1;
    }


    $RecvData = "$RecvData12$RecvData16$RecvData1A$RecvData1E";



    flash_green();

    # Beep the reader and turn the LED orange to indicate that we are good.
    ( undef, undef ) =
      $hCard->TransmitWithCheck( "FF 00 40 CF 04 04 00 01 03", "90 00" );

    if ($RecvData) {

        $datetime = localtime();
        chomp($datetime);

        my @data = map { chr hex $_ } split ' ', $RecvData;
	
        #regex out the data that we care about
        my $data = join( '', @data );
        my $attendeeid = $data;
        $attendeeid =~ s/^.*attendeeID=//o;
	
        # We only care about the 32 char MD5 string
        $attendeeid = substr( $attendeeid, 0, 32 );
	print "attendeeid is $attendeeid.\n";
        # If the attendee ID starts with "!!" then it's an admin tag.
        if ( $attendeeid =~ m/!!/ ) {
            admin_api($attendeeid);
        }

        print "recording last scan.\n";
        open( LASTSCAN, ">$cwd/settings/lastscan" );
        print LASTSCAN "$datetime";
        close(LASTSCAN);

        write_data( $attendeeid, $datetime );

    }

    $hCard->Disconnect();

}

print "done with parent\n";
exit;

sub write_data {

    open( FILE, ">>$cwd/tmp/$_[1]" );
    print FILE "AttendeeID:$_[0]";
    close(FILE);

    #    my $pid = fork();
    #    if (!defined $pid){
    #	die "cannot fork\n";
    #    }
    #print "renaming the file.\n";
    rename "$cwd/tmp/$_[1]", "$cwd/scans/$_[1]";
    print "Card Read.\n";

    exit 0;
}

sub admin_api {

    # Turn the buzzer off so we can control it directly with our script.
    ( undef, undef ) = $hCard->TransmitWithCheck( "FF 00 52 00 00", "00 90" );

    #die("Can't transmit data: $Chipcard::PCSC::errno") unless defined $sw;

    #	system ("sudo echo Entering Amin Interface|wall");
    $cwd = getcwd();
    if ( $_[0] =~ m/^!!reboot/ ) {

        system("sudo reboot");
        exit;

    }
    elsif ( $_[0] =~ m/^!!shutdown/ ) {

        system("sudo shutdown -h now");
        exit;

    }
    elsif ( $_[0] =~ m/^!!session/ ) {
        $sessionid = $_[0];
        $sessionid =~ s/^!!session=//o;
        $sessionid = substr( $sessionid, 0, 4 );

        open( SESSION, ">$cwd/settings/current_session" ) || die $!;
        print SESSION "$sessionid";
        close(SESSION);
        print "SessionID changed to $sessionid\n";
        exit;

    }
    elsif ( $_[0] =~ m/^!!vendor/ ) {
        $vendorid = $_[0];
        $vendorid =~ s/^!!vendor=//o;
        $vendorid = substr( $vendorid, 0, 4 );

        open( VENDOR, ">", "$cwd/settings/current_vendor" ) || die $!;
        print VENDOR "$vendorid";
        close(VENDOR);
        print "VendorID changed to $vendorid\n";
        exit;

    }
    elsif ( $_[0] =~ m/^!!voting/ ) {
        open( MODE, ">$cwd/settings/mode" ) || die $!;
        print MODE "voting";
        close(MODE);
        print "Mode changed to voting\n";
        exit;

    }
    elsif ( $_[0] =~ m/^!!attendance/ ) {
        open( MODE, ">$cwd/settings/mode" );
        print MODE "attendance";
        close(MODE);
        print "Mode changed to attendance\n";
        exit;
    }
    elsif ( $_[0] =~ m/^!!update/ ) {
        system("git pull");
        system("./setup.sh");
        system("sudo shutdown -h now");
        exit;
    }
    elsif ( $_[0] =~ m/^!!noled/ ) {
        system("touch $cwd/settings/noled");
    }
    elsif ( $_[0] =~ m/^!!led/ ) {
        unlink("$cwd/settings/noled");
    }
    elsif ( $_[0] =~ m/^!!overrideattendance/ ) {
        system("touch $cwd/settings/attendanceoverride");
    }


    return;
}

sub flash_green {

    my $pid = fork;
    return if $pid;
    system("./RPi-LPD8806/confirm.py");
    exit 0;

}

sub flash_red {

    my $pid = fork;
    return if $pid;
    system("./RPi-LPD8806/fail.py");
    exit 0;

}

sub reading_anim {

    my $pid = fork;
    return if $pid;
    system("./RPi-LPD8806/colorchase.py");
    exit 0;

}

# Mifare Ultralight example.  These are tiny block sizes.
#$cmd = Chipcard::PCSC::ascii_to_array("00 A4 04 00 0A A0 00 00 00 62 03 01 0C 06 01");
#$RecvData = $hCard->Transmit($cmd);
#die ("Can't transmit data: $Chipcard::PCSC::errno") unless defined $RecvData;
#print Chipcard::PCSC::array_to_ascii($RecvData)."\n";




# Working Topaz code

# This is complicated so I'll try to explain it...
#       [----------] Direct Transmit APDU - basically we are talking to the card.
#                   [--] The size of the tag command (13 bits)
#                       [------] Tag Command (InDataExchange)
#                               [--] Tag Command (Read Segment)
#                                  [--] Tag Address (00/10/20/30...)
#                                      [---------------------] Tag Data
#       [-----]  Expected result; fail if we don't get this message from the tag.
#    ( $sw, $RecvData00 ) = $hCard->TransmitWithCheck(
#        "FF 00 00 00 0D D4 40 01 10 00 00 00 00 00 00 00 00 00",
#        "90 00" );
#
#    unless ($sw) {
#        flash_red();
#        exit 1;
#    }
#
#    #print Chipcard::PCSC::Card::ISO7816Error($sw) . " ($sw)\n";
#
#    ( $sw, $RecvData10 ) = $hCard->TransmitWithCheck(
#        "FF 00 00 00 0D D4 40 01 10 10 00 00 00 00 00 00 00 00",
#        "90 00" );
#
#    unless ($sw) {
#        flash_red();
#        exit 1;
#    }
# $RecvData = "$RecvData00$RecvData10";
# $RecvData =~ s/55\ 55\ AA\ AA\ .*D5\ 41\ 00//;

